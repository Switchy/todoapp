<?php

class UsersTableSeeder extends Seeder {
	public function run() {

		DB::table('users')->delete();

		$users = array(
			array(
				'name' => 'Mike',
				'password' => Hash::make('1234'),
				'email' => 'switchytm@gmail.com'
			)
		);

		DB::table('users')->insert($users);
	}
}